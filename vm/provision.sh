#!/usr/bin/env bash
# Actual Script
echo "**************** Updating Repository ****************"
sudo apt-get update

echo "**************** Installing NGINX *******************"
sudo apt-get -y install nginx

echo "**************** Installing MySQL *******************"
echo "mysql-server mysql-server/root_password password root" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | sudo debconf-set-selections
sudo apt-get -y install mysql-server

echo "**************** Installing PHP *********************"
sudo apt-get -y install php-fpm php-mysql php-intl php-mbstring php-curl php-zip
sudo sed -i.bak 's/\;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/i' /etc/php/7.0/fpm/php.ini

echo "**************** Installing Composer *********************"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer

echo "**************** Enabling Site *******************"
sudo rm -rf /etc/nginx/sites-available/default
sudo ln -s "/var/www/efoos/config/www.dev.conf" "/etc/nginx/sites-available/efoos.dev.conf"
sudo ln -s "/etc/nginx/sites-available/efoos.dev.conf" "/etc/nginx/sites-enabled/efoos.dev.conf"

echo "*********** Installing NPM and Related Tools **************"
sudo apt-get install python-software-properties
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt-get install nodejs
sudo npm install --global gulp-cli

echo "************* Updating database *****************"
cd /var/www/efoos/data/www
cp config.php.sample config.php

composer install
cd /var/www/efoos/sql/
mysql -uroot -proot < eFoos.sql
php apply-patches.php