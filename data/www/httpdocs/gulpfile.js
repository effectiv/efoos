'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
 
gulp.task('sass', function () {
  return gulp.src('./assets/gridberg.2.1/SCSS/gridberg.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./assets/gridberg.2.1/CSS'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./assets/gridberg.2.1/SCSS/*.scss', ['sass']);
});
